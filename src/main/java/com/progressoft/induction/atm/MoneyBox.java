package com.progressoft.induction.atm;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class MoneyBox {
    private Map<Banknote, BigDecimal> numberOfBanknotes;

    public MoneyBox(){
        numberOfBanknotes = new HashMap<>();
        numberOfBanknotes.put(Banknote.FIFTY_JOD, new BigDecimal("10"));
        numberOfBanknotes.put(Banknote.TWENTY_JOD, new BigDecimal("20"));
        numberOfBanknotes.put(Banknote.TEN_JOD, new BigDecimal("100"));
        numberOfBanknotes.put(Banknote.FIVE_JOD, new BigDecimal("100"));
    }

    public void addBanknotes(Banknote banknoteType, BigDecimal amount){
        validateAmount(amount);
        numberOfBanknotes.put(banknoteType, numberOfBanknotes.get(banknoteType).add(amount));
    }

    public void removeBanknote(Banknote banknoteType, BigDecimal amount){
        validateAmount(amount);
        if(numberOfBanknotes.get(banknoteType).compareTo(amount)<0){
            throw new IllegalArgumentException(String.format("money box doesn't have enough to get this amount[%s]"+
                    "of banknote[%s]",amount,banknoteType));
        }
        numberOfBanknotes.put(banknoteType,numberOfBanknotes.get(banknoteType).subtract(amount));
    }

    private void validateAmount(BigDecimal amount){
        if(amount == null) {
            throw new NullPointerException("amount value is null");
        }
        if(amount.compareTo(BigDecimal.ZERO) < 0){
            throw new IllegalArgumentException("amount value is less than zero, amount: " +amount);
        }
    }

    public BigDecimal totalBanknoteValue(){
        BigDecimal sum = new BigDecimal("0");
        for(Banknote banknote:Banknote.values()){
            sum = sum.add(banknote.getValue().multiply(numberOfBanknotes.get(banknote)));
        }
        return sum;
    }

    public BigDecimal checkNumberOfBanknotes(Banknote banknote){ return numberOfBanknotes.get(banknote);}
}
