package com.progressoft.induction.atm;

import com.progressoft.induction.atm.exceptions.InsufficientFundsException;
import com.progressoft.induction.atm.exceptions.NotEnoughMoneyInATMException;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class SimpleATM implements ATM {
    private BankingSystem bankingSystem;
    private MoneyBox moneyBox;

    public SimpleATM(BankingSystem bankingSystem, MoneyBox moneyBox){
        if(bankingSystem == null || moneyBox == null){
            throw new NullPointerException(String.format("all/some of values are null, bankingSystem[%s] "+
                    "moneyBox[%s]",bankingSystem,moneyBox));
        }
        this.bankingSystem = bankingSystem;
        this.moneyBox = moneyBox;
    }

    @Override
    public List<Banknote> withdraw(String accountNumber, BigDecimal amount) {
        if(amount == null) {
            throw new NullPointerException("amount value is null, " + amount);
        }

        if(bankingSystem.getAccountBalance(accountNumber).compareTo(amount) < 0){
            throw new InsufficientFundsException();
        }

        if(moneyBox.totalBanknoteValue().compareTo(amount) < 0){
            throw new NotEnoughMoneyInATMException();
        }

        bankingSystem.debitAccount(accountNumber, amount);

        List<Banknote> withdrawnBanknotes = new ArrayList<>();

        while (amount.compareTo(BigDecimal.ZERO) != 0 ){
            for(Banknote banknote: Banknote.values()){
                if(amount.compareTo(banknote.getValue())<0)
                    continue;
                if(moneyBox.checkNumberOfBanknotes(banknote).compareTo(BigDecimal.ZERO)>0){
                    withdrawnBanknotes.add(banknote);
                    moneyBox.removeBanknote(banknote, new BigDecimal(1));
                    amount = amount.subtract(banknote.getValue());
                }
            }
        }
        return withdrawnBanknotes;
    }
}
