package com.progressoft.induction.atm;

import com.progressoft.induction.atm.exceptions.AccountNotFoundException;

import java.math.BigDecimal;

public class SimpleBankingSystem implements BankingSystem {
    private com.progressoft.induction.atm.FakeRepository fakeRepository;

    public SimpleBankingSystem(){ fakeRepository = new com.progressoft.induction.atm.FakeRepository();}

    @Override
    public BigDecimal getAccountBalance(String accountNumber) {
        // I didn't check if accountNumber is null or not because fakeRepository will do so
        BankAccount bankAccount = fakeRepository.getAccount(accountNumber);
        validateAccount(bankAccount);
        return bankAccount.getMoneyBalance();
    }

    @Override
    public void debitAccount(String accountNumber, BigDecimal amount) {
        BankAccount bankAccount = fakeRepository.getAccount(accountNumber);
        validateAccount(bankAccount);
        bankAccount.withdraw(amount);
    }

    private void validateAccount(BankAccount bankAccount){
        if(bankAccount == null){
            throw new AccountNotFoundException();
        }
    }
}
