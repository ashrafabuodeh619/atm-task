package com.progressoft.induction.atm;

import com.progressoft.induction.atm.exceptions.InsufficientFundsException;

import java.math.BigDecimal;

public class BankAccount {
    private BigDecimal moneyBalance;
    private  final String accountNumber;
    private String owner;

    public BankAccount(BigDecimal moneyBalance , String accountNumber , String owner){
        if(accountNumber == null || owner == null){
            throw new NullPointerException(String.format("all/some values are null ,moneyBalance[%s]"+
                    "accountNumber[%s] owner[%s]",moneyBalance,accountNumber,owner));

        }
        validateMoneyAmount(moneyBalance);
        this.moneyBalance = moneyBalance;
        this.accountNumber = accountNumber;
        this.owner = owner;
    }

    public BigDecimal getMoneyBalance() { return moneyBalance; }

    public String getAccountNumber() { return accountNumber; }

    public String getOwner() { return owner; }

    public void withdraw(BigDecimal moneyAmount){
        validateMoneyAmount(moneyAmount);
        if(moneyAmount.compareTo(moneyBalance) > 0){
            throw new InsufficientFundsException();
        }
        moneyBalance = moneyBalance.subtract(moneyAmount);
    }

    public void deposit(BigDecimal moneyAmount){
        validateMoneyAmount(moneyAmount);
        moneyBalance = moneyBalance.add(moneyAmount);

    }

    private void validateMoneyAmount(BigDecimal moneyAmount){
        if(moneyAmount == null) {
            throw new NullPointerException("moneyAmount value is null");
        }

        if(moneyAmount.compareTo(BigDecimal.ZERO)<0){
            throw new IllegalArgumentException("moneyAmount is negative,moneyAmount: "+moneyAmount);

        }
    }
}
