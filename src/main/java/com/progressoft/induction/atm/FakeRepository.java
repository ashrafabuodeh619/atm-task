package com.progressoft.induction.atm;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class FakeRepository {
    private List<BankAccount> bankAccounts;

    public FakeRepository(){
        bankAccounts = new ArrayList<>();
        bankAccounts.add(new BankAccount(new BigDecimal("1000.0"),"123456789","ashraf"));
        bankAccounts.add(new BankAccount(new BigDecimal("1000.0"),"111111111","omar"));
        bankAccounts.add(new BankAccount(new BigDecimal("1000.0"),"222222222","saif"));
        bankAccounts.add(new BankAccount(new BigDecimal("1000.0"),"333333333","yazeed"));
        bankAccounts.add(new BankAccount(new BigDecimal("1000.0"),"444444444","zaid"));
    }

    public BankAccount getAccount(String accountNumber){
        if(accountNumber == null){
            throw new NullPointerException("accountNumber value is null");
        }
        for(BankAccount bankAccount:bankAccounts){
            if(accountNumber.equals(bankAccount.getAccountNumber()))
                return bankAccount;

        }
        return null;
    }
}
